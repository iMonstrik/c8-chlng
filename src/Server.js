const net = require('net');

const {Request} = require('./Request');
const {Response} = require('./Response');

class Server {

    constructor(port, address, router, debug = true) {
        this.port = port;
        this.address = address;
        this.router = router;
        this.debug = debug;
    }

    serve() {

        this.server = net.createServer(this.handleConnection.bind(this));
        this.server.on('close', process.exit.bind(null, 1));
        this.server.listen(this.port, this.address);
        console.log('server listening on ' + this.port, this.address);
    }

    handleConnection(socket) {
        let rawRequest = '';
        let request;
        let doneReading = false;
        socket.on('data', (data) => {
            if (doneReading) {
                return;
            }

            rawRequest += data;

            try {
                request = Request.unmarshall(rawRequest);
                const {method} = request;
                const contentLength = +request.headers['content-length'];
                if ((method === 'GET' && rawRequest.endsWith('\r\n\r\n')) ||
                    (request.body.length === contentLength)) {
                    doneReading = true;
                }
            } catch (e) {
                // Not done reading
            }

            if (doneReading) {
                done();
            }
        });

        const {router, debug} = this;

        function done() {
            if (debug) {
                console.log(rawRequest.trim() + '\n\n');
            }

            const response = router.handleRequest(request);
            const rawResponse = response.marshall();
            socket.end(rawResponse);
        }
    }
}

module.exports.Server = Server;
