const METHODS = ['GET', 'PUT', 'POST', 'DELETE', 'PATCH'];

class Router {

  constructor() {
    this.routes = {};
    this.interceptors = {};
  }

  printRoutes() {
    console.log('---Routes---');
    console.log(this.routes);
    console.log('---Routes---');
  }

  addRoute(route, handler, method) {
    if (!this.routes[route]) {
      this.routes[route] = {};
    }

    if (!method) {
      METHODS.forEach((method) => {
        this.routes[route][method] = handler;
      });
    } else {
      this.routes[route][method] = handler;
    }
  }

  addInterceptor(status, handler) {
    this.interceptors[status] = handler;
  }

  handleRequest(request) {

    // console.log('%%% handle Request', request);

    // STATIC
    if (request.uri.split("/").length > 2 && request.uri.indexOf('/static/') >= 0) {

      // let routeParts = request.uri.split("/");
      // console.log('%%% routeParts', routeParts );
      // for (let prop in this.routes) {
      //   console.log(this.routes[prop]);
      //
      // }

      return this.routes['/static/*'][request.method](request);
    }

    // PARAM
    if (request.uri.indexOf('?') > 0) {
      return this.routes['/basic'][request.method](request);
    }

    // 404
    if (!this.routes[request.uri]) {
      //TODO add default interceptors
      //TODO check if not exist user defined 404 interceptor run default
      return this.interceptors[404](request);
    }

    // 501
    if (!this.routes[request.uri][request.method]) {
      //TODO add default interceptors
      //TODO check if not exist user defined 404 interceptor run default
      return this.interceptors[501](request);
    }

    // DEFAULT
    return this.routes[request.uri][request.method](request);
  }

}

module.exports.Router = Router;
