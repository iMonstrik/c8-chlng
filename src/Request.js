class Request {
  static unmarshall(requestString) {
    const requestLines = requestString.split('\r\n');
    const [method, uri, version] = requestLines[0].split(' ');
    const endOfHeaders = requestLines.indexOf('');
    const headerLines = requestLines.slice(1, endOfHeaders - 1);
    const headers = {};
    headerLines.forEach(function (headerLine) {
      const [header, value] = headerLine.split(':');
      headers[header.toLowerCase().trim()] = value.trim();
    });
    const body = requestLines.slice(endOfHeaders).join('\r\n').trim();
    return new Request(method, uri, version, headers, body);
  }

  constructor(method, uri, version, headers, body) {
    this.method = method;
    this.uri = uri;
    this.version = version;
    this.body = body;
    this.headers = headers;
  }
}

module.exports.Request = Request;
