const fs = require('fs');
const path = require('path');

const {Server} = require('./Server');
const {Router} = require('./Router');
const {Response} = require('./Response');



// HANDLERS
function handleBasic(request) {
   const body = 'Hello World!';
  const headers = {
    'Content-Length': body.length // 'Content-Type': 'text/html'
  };

  return new Response('HTTP/1.1', 200, 'OK', headers, body);
}

function handle404(request) {
  // TODO: Add logs
  return new Response('HTTP/1.1', 404, 'Not Found', {}, '');
}

function handle501(request) {
  // TODO: Add logs
  return new Response('HTTP/1.1', 501, 'Not Implemented', {}, '');
}

function handleUser(request) {
  // Should return a message (Hello foo!) where foo is replaced with the <user> variable.
  return handle501();
}

function handleStatic(request) {
  // Should return the static file found in the adjacent directory with content-type.
  const fileName = request.uri.substr(request.uri.indexOf('/static/') + 8);
  console.log('%%% handleStatic fileName ', fileName);

  // fs.readdir(path.resolve(__dirname, '..', 'static/'), function (err, items) {});

  //TODO: add more specific error handling
  try {
    const body = fs.readFileSync(path.resolve(__dirname, '..', `static/${fileName}`));
    const headers = {
      'content-length': [body.length.toString()],
      'content-type': [{html: 'text/html', css: 'text/css'}[fileName.split('.')[1]]],
    };
    return new Response('HTTP/1.1', 200, 'OK', headers, body);

  } catch (err) {
    // console.log('%%% handleStatic readFileSync error ', err);
    return handle404();
  }

}

function handleUserCreate(request) {
  // Should return the new user's username given a JSON request like { username: 'foo' }
  const requestBody = JSON.parse(request.body);

  //TODO: create user
  const body = requestBody.username;
  const headers = {
    'Content-Length': body.length.toString(),
  };
  return new Response('HTTP/1.1', 200, 'OK', headers, body);

}



// MAIN
function main() {
  const router = new Router();

  router.addRoute('/basic', handleBasic);
  router.addRoute('/404', handle404);
  router.addRoute('/users/<user>/message', handleUser);
  router.addRoute('/static/*', handleStatic);
  router.addRoute('/users', handleUserCreate, 'POST');

  router.addInterceptor(404, handle404);
  router.addInterceptor(501, handle501);

  const server = new Server('8080', 'localhost', router,);
  server.serve();
  // router.printRoutes();
}

main();
