class Response {
  constructor(version, status, reason, headers, body) {
    this.version = version;
    this.status = status;
    this.reason = reason;
    this.headers = headers;
    this.body = body;
  }

  marshall() {
    const statusLine = `${this.version} ${this.status} ${this.reason}`;
    const headers = Object
      .keys(this.headers)
      .reduce((accum, header) => [...accum, `${header}: ${this.headers[header]}`], [])
      .join('\r\n');
    const body = this.body.length ? `\r\n${this.body}` : '';
    return [statusLine, headers, body].join('\r\n');
  }
}

module.exports.Response = Response;
