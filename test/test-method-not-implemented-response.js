module.exports.run = async function () {
  const EXPECTED = {
    body: '',
    status: '501',
    reason: 'Not Implemented',
    headers: {},
  };

  const response = await request(`${SERVER}/users`);
  await assertResponse(EXPECTED, response);
};
