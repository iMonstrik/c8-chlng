module.exports.run = async function () {
  const EXPECTED = {
    body: 'Hello World!',
    status: '200',
    reason: 'OK',
    headers: {
      'content-length': ['12'],
    },
  };

  console.log('test-basic-query-parameter-response');
  const response = await request(`${SERVER}/basic?thisshouldnotfail=1`);
  await assertResponse(EXPECTED, response);
};
