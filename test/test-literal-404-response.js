module.exports.run = async function () {
  const EXPECTED = {
    body: '',
    status: '404',
    reason: 'Not Found',
    headers: {},
  };

  const response = await request(`${SERVER}/404`);
  await assertResponse(EXPECTED, response);
};
