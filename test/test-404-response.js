module.exports.run = async function () {
  const EXPECTED = {
    body: '',
    status: '404',
    reason: 'Not Found',
    headers: {},
  };

  const response = await request(`${SERVER}/${randomValueHex(16)}`);
  await assertResponse(EXPECTED, response);
};
