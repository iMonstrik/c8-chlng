module.exports.run = async function () {
  const username = randomValueHex(32);
  const user = { username };

  const EXPECTED = {
    body: username,
    status: '200',
    reason: 'OK',
    headers: {
      'content-length': [username.length.toString()],
    },
  };

  const response = await request(`${SERVER}/users`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify(user),
  });
  await assertResponse(EXPECTED, response);
};
