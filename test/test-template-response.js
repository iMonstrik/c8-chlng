module.exports.run = async function () {
  const user = randomValueHex(32);
  const body = `Hello ${user}!`;

  const EXPECTED = {
    body,
    status: '200',
    reason: 'OK',
    headers: {
      'content-length': [body.length],
    },
  };

  const response = await request(`${SERVER}/users/${user}/message`);
  await assertResponse(EXPECTED, response);
};
