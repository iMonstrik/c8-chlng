const assert = require('assert');
const crypto = require('crypto');

const fetch = require('node-fetch');

global.request = fetch;
global.SERVER = 'http://localhost:8080';

global.assertResponse = async function (expected, response) {
  assert.deepEqual({
    body: (await response.text()) || '',
    status: response.status.toString(),
    reason: response.statusText,
    headers: keysToLowerCase(response.headers.raw()),
  }, {
    ...expected,
    body: expected.body.toString(),
    status: expected.status.toString(),
  });
};

global.randomValueHex = function (len) {
  return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').slice(0, len);
}

function keysToLowerCase(map) {
  return Object.keys(map).reduce((accum, key) => ({...accum, [key]: map[key]}), {});
};

const tests = [
  'basic-response',
  'literal-404-response',
  '404-response',
  'basic-query-parameter-response',
  'method-not-implemented-response',
  'post-response',
  'content-type-response',
  // 'template-response',
];

async function main() {
  try {
    await Promise.all(tests.map(async function (test) {
      console.log(`Running ${test} tests...`);
      await require(`./test-${test}`).run();
      console.log(`Finished ${test} successfully...`);
    }));

    console.log('\nAll tests passed!\n');
  } catch (e) {
    console.error(e);
    console.log('\nTests failed!\n');
  }
}

main();
