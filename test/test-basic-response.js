module.exports.run = async function () {
  const EXPECTED = {
    body: 'Hello World!',
    status: '200',
    reason: 'OK',
    headers: {
      'content-length': ['12'],
    },
  };

  const response = await request(`${SERVER}/basic`);
  await assertResponse(EXPECTED, response);
};
