const fs = require('fs');
const path = require('path');

module.exports.run = function () {
  const files = ['index.html', 'blog.html', 'style.css'];

  return Promise.all(files.map(async function (file) {
    const body = fs.readFileSync(path.resolve(__dirname, '..', `static/${file}`));
    const EXPECTED = {
      body,
      status: '200',
      reason: 'OK',
      headers: {
        'content-length': [body.length.toString()],
        'content-type': [{ html: 'text/html', css: 'text/css' }[file.split('.')[1]]],
      },
    };

    const response = await request(`${SERVER}/static/${file}`);
    return assertResponse(EXPECTED, response);
  }));
};
