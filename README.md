# Interview challenge

This project contains a socket server implementing the HTTP/1.1
protocol. 
The server logic, request data logic, response data logic,
and router logic is in [src/Server.js](src/Server.js),
[src/Request.js](src/Request.js), [src/Response.js](src/Response.js),
[src/Router.js](src/Router.js), respectively. 
The main entrypoint is
in [src/index.js](src/index.js).

## Setup

Install Node (at least 8.9.0) and npm. Run `npm install` in this
directory to install the dependencies (only dependencies are for
tests).

## Your task

Your task is to extend the given logic to support a few router
features:

* Basic response headers (e.g. `content-length`, `content-type`)
* Basic query parameter support
* Routing based on method
* Route variables (e.g. `/users/<username>/message`)
* Variable suffix (e.g. `/static/*`)
* Error handlers handlers (e.g. 404 for missing routes)
* Any other internal abstractions you find helpful

You should be able to support the declared routes in `src/index.js`
but you are free to modify any internals so long as you pass
integration tests.

Run `yarn start` to start the server.

## Integration tests

Integration tests for all the cases have been provided and can be run
via `yarn test`.

## Notes

For the sake of this exercise, try not to include 3rd-party
libraries. We'd like to see you work.

## Bonus points

Extend the `handleStatic` and associated integration tests to support
the "Last-Modified" header based off of the requested file's last
modified time.